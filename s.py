import time  
import requests  
from time import gmtime, strftime  
  
def resulting(c1,c2,c3):  
  url = "https://data.messari.io/api/v2/assets?with-profiles&limit=90"  
  res = requests.get(url)  
  rj = res.json()['data']  
  
  res = requests.get(url)  
  tt = strftime("%Y-%m-%d %H:%M:%S", gmtime(time.time()+14400))  
  st = str(tt)+'\n\n'  
  try:  
    rj = res.json()['data']  
    rt = ''  
  except:  
    rj = []  
  sl = []  
  pl = []  
  l2 = []  
  l1 = []  
  ll = []  
  l3 = []  
  
  for i in range(len(rj)):  
    sym = rj[i]['symbol']  
    pri = rj[i]['metrics']['market_data']['price_usd']  
    c24 = rj[i]['metrics']['market_data']['percent_change_btc_last_24_hours']  
    c1h = rj[i]['metrics']['market_data']['percent_change_btc_last_1_hour']  
    c1w = rj[i]['metrics']['roi_data']['percent_change_btc_last_1_week']  
    try:  
      if 'US' not in sym and "DAI" not in sym and c1h:  
        if c24>0 and c1w>0:  
          sl.append(sym)  
          pl.append(pri)  
          l2.append(c24-c1h/2)  
          l1.append(c1h)  
          l3.append(c1w-c24)  
      elif 'BUSD' in sym:  
          sl.append(sym)  
          pl.append(pri)  
          l2.append(c24-c1h/2)  
          l1.append(c1h)  
          l3.append(c1w-c24)  
    except:  
      pass  
  
  for i in range(len(l2)):  
    l11 = (l1[i]-(sum(l1)/len(l1)))/(max(l1)-min(l1))  
    l22 = (l2[i]-(sum(l2)/len(l2)))/(max(l2)-min(l2))  
    l33 = (l3[i]-(sum(l3)/len(l3)))/(max(l3)-min(l3))  
    ll.append((l33*c3+l22*c2+l11*c1)*10)  
          
  ll, l1, l3, pl, l2, sl = zip(*sorted(zip(ll, l1, l3, pl, l2, sl),reverse=True))  
  
  llr = []  
  lls = []  
  
  busdi = 0  
  for i in range(len(ll)):  
    if ll[i]<0:  
      busdi = i  
      break  
    elif sl[i]=='BUSD':  
      busdi = i+1  
      break  
  
  
  n=10  
  ll = ll[:min(busdi,n)]  
    
  if (len(ll)<2 or sum(l1)<-50) and True:  
    rt += 'Just keep USD'  
  else:  
    for i in range(min(len(ll),n)):  
        lls.append(sl[i])  
        llr.append(ll[i])  
  
        rt += str(i+1)+'   '+sl[i][:5]+' '*(6-len(sl[i]))+'        '+str(l1[i])[:6]+'       '+str(l2[i])[:6]+'       '+str(l3[i])[:6]+'        '+str(round(ll[i]*100/sum(map(abs, ll))))[:5]+'%      '  
  
        # if ll[i]>20:  
        #     rt += '$ '  
        # elif ll[i]>10:  
        #     rt += '* '  
        # elif ll[i]>5:  
        #     rt += '! '  
        # elif ll[i]>0:  
        #     rt += ' '  
        rt += '\n'  
    
  
  
  rt += '\n\none hour avg: '+str(round(sum(l1)))+'\n'  
  return rt  
  
def send_msg(text):  
   token = "2108947054:AAEMnecTDV_H3z4tm1ZAGe1j7mG7IdnriGI"  
   chat_id = "-1001503993816"  
   url_req = "https://api.telegram.org/bot" + token + "/sendMessage" + "?chat_id=" + chat_id + "&text=" + text   
   results = requests.get(url_req)  
  
def edit_msg(text,message_id):  
   token = "2108947054:AAEMnecTDV_H3z4tm1ZAGe1j7mG7IdnriGI"  
   chat_id = "-1001503993816"  
   url_req = "https://api.telegram.org/bot" + token + "/editMessageText" + "?chat_id=" + chat_id + "&message_id=" + message_id + "&text=" + text   
   results = requests.get(url_req)  
  
def whiling(t,c1,c2,c3,mi):  
  s = ''  
  tt = ''  
  try:  
    tt = strftime("%Y-%m-%d %H:%M:%S", gmtime(time.time()+14400))  
    s='\n\n'+resulting(c1,c2,c3)  
    edit_msg(t+tt+s,mi)  
  except Exception as e:  
    if len(s)>0:  
      edit_msg(t+tt+s+"\n\n"+str(e)+"\t\tnot refreshing...",mi)  
    else:  
      pass  
  
  
def home():  
  while True:  
    whiling('Short term:\n\n',7,5,1,'9')  
    time.sleep(10)  
    whiling('Long term:\n\n',1,5,7,'10')  
    time.sleep(10)  
      
home()
